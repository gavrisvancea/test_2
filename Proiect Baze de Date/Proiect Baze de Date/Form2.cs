﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proiect_Baze_de_Date
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AS1 as_1 = new AS1();
            as_1.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AS2 as_2 = new AS2();
            as_2.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            AS3 as_3 = new AS3();
            as_3.Show();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            AS4 as_4 = new AS4();
            as_4.Show();

        }
    }
}
