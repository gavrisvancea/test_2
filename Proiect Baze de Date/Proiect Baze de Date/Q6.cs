﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace Proiect_Baze_de_Date
{
    public partial class Q6 : Form
    {
        public Q6()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MySqlConnection connection = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=proiect bd");

            connection.Open();

            string query = "SELECT p.nume FROM PROPRIETAR p, APARTAMENT a, APARTAMENT a2 WHERE a.id_proprietar = a2.id_proprietar AND a.id_ap < a2.id_ap AND p.nume != ANY (SELECT p.nume FROM proprietar p, apartament a1, apartament a2 WHERE a1.id_proprietar = a2.id_proprietar) ";

            MySqlCommand commandDatabase = new MySqlCommand(query, connection);

            MySqlDataReader reader;

            try
            {
                //connection.Open();
                reader = commandDatabase.ExecuteReader();

                // If there are available rows

                if (reader.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;



                    while (reader.Read())
                    {



                        string[] row = new string[1];

                        for (int k = 0; k < 1; k++)
                            row[k] = reader.GetString(k);

                        ListViewItem listViewItem1 = new ListViewItem(row);
                        listView1.Items.Add(listViewItem1);

                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();

        }

       
    }
}
