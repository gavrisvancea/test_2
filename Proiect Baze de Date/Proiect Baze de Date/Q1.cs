﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace Proiect_Baze_de_Date
{
    public partial class Q1 : Form
    {
        public Q1()
        {
            InitializeComponent();

            MySqlConnection connection = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=proiect bd");
            connection.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            MySqlConnection connection = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=proiect bd");

            connection.Open();

            //string query = "SELECT * FROM PROPRIETAR WHERE nume LIKE '%" + textBox1.Text+ "' ORDER BY nume DESC";


           // string query = "EXECUTE proc1 @litera='a'";


            MySqlCommand commandDatabase = new MySqlCommand("proc1", connection);

            commandDatabase.CommandType = CommandType.StoredProcedure;
            commandDatabase.Parameters.AddWithValue("@litera", textBox1.Text);

            MySqlDataReader reader;
            
            try
            {
                //connection.Open();
                reader = commandDatabase.ExecuteReader();

                // If there are available rows

                if (reader.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;



                    while (reader.Read())
                    {

              

                        string[] row = new string[3];

                        for (int k = 0; k < 3; k++)
                            row[k] = reader.GetString(k);

                        ListViewItem listViewItem1 = new ListViewItem(row);
                        listView1.Items.Add(listViewItem1);

                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }
    }
}
