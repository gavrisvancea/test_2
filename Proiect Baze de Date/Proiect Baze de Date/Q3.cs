﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace Proiect_Baze_de_Date
{
    public partial class Q3 : Form
    {
        public Q3()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MySqlConnection connection = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=proiect bd");

            connection.Open();

            string query = "SELECT ap.adresa, ap.nr_ap, pro.nume, co.valoare FROM((APARTAMENT ap INNER JOIN PROPRIETAR pro ON ap.id_proprietar = pro.id_proprietar) INNER JOIN CONSUM co ON co.id_ap = ap.id_ap) WHERE ap.adresa = '" + textBox3.Text + " AND co.luna = '" + textBox1.Text + "'" + "";

            MySqlCommand commandDatabase = new MySqlCommand(query, connection);

            MySqlDataReader reader;

            try
            {
                //connection.Open();
                reader = commandDatabase.ExecuteReader();

                // If there are available rows

                if (reader.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;



                    while (reader.Read())
                    {



                        string[] row = new string[4];

                        for (int k = 0; k < 4; k++)
                            row[k] = reader.GetString(k);

                        ListViewItem listViewItem1 = new ListViewItem(row);
                        listView1.Items.Add(listViewItem1);

                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }
    }
}
