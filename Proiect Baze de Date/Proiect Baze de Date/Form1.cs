﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;



namespace Proiect_Baze_de_Date
{
    public partial class Form1 : Form
    {

        

        public Form1()
        {
            InitializeComponent();
           
            MySqlConnection connection = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=proiect bd");
            connection.Open();

            if (connection.State == ConnectionState.Open)
            {
                label1.Text = "Connected";
                label1.ForeColor = Color.Green;
            }
            else
            {
                label1.Text = "Disconnected";
                label1.ForeColor = Color.Red;
            }


        }



        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();  
        }


        // Select all rows from a specified tabel


        private void button3_Click(object sender, EventArgs e)
        {
            MySqlConnection connection = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=proiect bd");

            connection.Open();

            string query = "SELECT * FROM " + textBox1.Text + "";

            MySqlCommand commandDatabase = new MySqlCommand(query, connection);

            MySqlDataReader reader;

            try
            {
                //connection.Open();
                reader = commandDatabase.ExecuteReader();

                // If there are available rows
                int i = 0;

                i = 3;

                if (reader.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;

                    

                    while (reader.Read())
                    {

                        int index = 0;

                        if (textBox1.Text == "proprietar")
                            index = 3;
                        else if (textBox1.Text == "apartament")
                            index = 5;
                        else if (textBox1.Text == "consum")
                            index = 7;
                        else if (textBox1.Text == "chitanta")
                            index = 4;

                        string[] row = new string[index];

                        for (int k = 0; k < index; k++)
                            row[k] = reader.GetString(k);

                        ListViewItem listViewItem1 = new ListViewItem(row);
                        listView1.Items.Add(listViewItem1);

                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form2 form_2 = new Form2();
            form_2.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form3 form_3 = new Form3();
            form_3.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form4 form_4 = new Form4();
            form_4.Show();

        }
    }
}
