﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_Baze_de_Date
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Q1 q_1 = new Q1();
            q_1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Q2 q_2 = new Q2();
            q_2.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Q3 q_3 = new Q3();
            q_3.Show();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Q4 q_4 = new Q4();
            q_4.Show();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Q5 q_5 = new Q5();
            q_5.Show();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Q6 q_6 = new Q6();
            q_6.Show();

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Q7 q_7 = new Q7();
            q_7.Show();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Q8 q_8 = new Q8();
            q_8.Show();

        }
    }
}
